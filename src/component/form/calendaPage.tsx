"use client";
import { Carousel, Image } from "antd";
import { motion, useMotionValue, useTransform, animate } from "framer-motion";
import { useEffect } from "react";
import { ReactTyped } from "react-typed";

export default function Calendar({ date, tab, setTab, imagePng }: { date: Date; tab: any; setTab: any; imagePng: string }) {
  const start = date.getFullYear(),
    end = new Date().getFullYear();
  const count = useMotionValue(start);
  const rounded = useTransform(count, Math.round);

  useEffect(() => {
    const animation = animate(count, end, {
      duration: 10,
      delay: 5,
    });
  }, [count, end]);
  return (
    <>
      <div
        className="flex flex-col justify-end"
        style={{
          background: "url('/images/calenda.png') top right/100% 100% no-repeat",
          zIndex: 100,
          position: "absolute",
          width: "150px",
          height: "150px",
          right: "10px",
          padding: "10px",
          top: "10%",
          animation: "ring 9s linear infinite alternate",
        }}
      >
        <div
          className=" bg-neutral-500 flex"
          style={{
            backgroundColor: "whitesmoke",
            margin: "10px",
            height: "60%",
            borderRadius: "0 0 10px 10px",
            filter: "drop-shadow(5px 15px 5px #222)",
          }}
        >
          <div className="flex flex-col text-black !pl-[10px]" style={{ paddingLeft: "10px", paddingRight: "10px" }}>
            <h4 style={{ fontSize: "20px !important", textAlign: "right" }}>{new Intl.DateTimeFormat("en-US", { month: "long" }).format(date)}</h4>
            <h1 style={{ fontSize: "30px !important" }}>{date.getDate()}</h1>
          </div>
          <div className=" text-black !pl-[10px]" style={{ paddingTop: "15%", borderLeft: "1px black solid", height: "fit" }}>
            <motion.h1 style={{ fontSize: "30px !important", textAlign: "right", rotate: "90deg" }}>{rounded}</motion.h1>
          </div>
        </div>
      </div>
      <div
        id="content-first"
        style={{ zIndex: 1000, position: "absolute", top: "37%", left: "20%" }}
        className="content p-3 ant-popover ant-popconfirm ant-popover-placement-top bg-white text-black"
      >
        <ReactTyped
          strings={[
            `Và ngày này ${
              end - start
            } năm trước,Trong ánh nắng ấm áp buổi ban mai, và tiếng hoan ca của đát tròi, một thiên thần nhỏ đã đến làm cho thế giới này muôn sắc hơn.`,
            "và một ngày Pé thiên thần của anh đã xuất hiện,trong ánh mắt và tâm trí của ang, làm tô vẽ nên tương lai rực rỡ của anh. .",
            "Thiên thần pé nhỏ của anh hãy cười thật tươi trong ngày tuyệt vời này ha 😘🥰😍😍 ",
          ]}
          typeSpeed={90}
          startDelay={10000}
          backDelay={5000}
          onComplete={() => {
            setTimeout(() => {
              setTab(tab + 1);
            }, 15000);
          }}
        />
      </div>
      <div className="cloud " style={{ bottom: "15%", left: "25%", zIndex: 1 }}>
        <Carousel autoplay fade infinite={false} dots={false}>
          <div>
            <Image
              alt="cloud"
              style={{
                bottom: "0%",
                width: "200px",
                height: "300px",
                filter: "drop-shadow(5px 15px 5px #222)",
                animation: ` ring 15s linear infinite alternate`,
              }}
              preview={false}
              src="/images/baby.png"
            />
          </div>
          <div>
            <Image
              alt="cloud"
              style={{
                bottom: "0%",
                width: "200px",
                height: "300px",
                filter: "drop-shadow(5px 15px 5px #222)",
                animation: ` ring 15s linear infinite alternate`,
              }}
              preview={false}
              src="/images/child.png"
            />
          </div>
          <div>
            <Image
              alt="cloud"
              style={{
                bottom: "0%",
                width: "200px",
                height: "300px",
                filter: "drop-shadow(5px 15px 5px #222)",
                animation: ` ring 15s linear infinite alternate`,
              }}
              preview={false}
              src="/images/student.png"
            />
          </div>
          <div style={{ height: "100%" }}>
            <Image
              alt="cloud"
              style={{
                bottom: "0%",
                width: "200px",
                height: "300px",
                filter: "drop-shadow(5px 15px 5px #222)",
                animation: ` ring 15s linear infinite alternate`,
              }}
              preview={false}
              src={imagePng}
            />
          </div>
        </Carousel>
      </div>
    </>
  );
}
