"use client";

import { EnvironmentFilled, FacebookFilled, MailFilled, PhoneFilled } from "@ant-design/icons";
import { Checkbox, Form, Input, Select } from "antd";
import TextArea from "antd/es/input/TextArea";
import emailJs from "@emailjs/browser";
import React, { useState } from "react";
import styled from "styled-components";
interface FormInit {
  message: string;
}
const formInit: FormInit = {
  message: "",
};
interface sendEmail {
  toEmail: string;
  fromName: string;
  reply_to: string;
  name: string;
  message: string;
}
const StyledInput = styled(TextArea)`
  &::placeholder {
    background-image: -webkit-linear-gradient(180deg, #ffffff 0%, #6284ff 33%, #ff003c 66%, #ffffff 100%);
    background-clip: text;
    -webkit-text-fill-color: transparent;
  }
`;
export default function Promise({ name, email, onClick }: { name: string; email?: string; onClick: any }) {
  const [formData, setFormData] = useState(formInit);
  const [form] = Form.useForm();
  const [loading, setloading] = useState(false);

  function sendEmail(e: FormInit) {
    if (email && email !== "") {
      setloading(true);
      emailJs.init({
        publicKey: "9jPox_Plx0-q3Za0C",
      });
      const senAdmin: sendEmail | any = {
        toEmail: "thienlytky123@gmail.com",
        fromName: email,
        reply_to: "",
        name: name,
        message: JSON.stringify(e),
      };
      if (senAdmin.toEmail !== "") {
        emailJs.send("service_u335ylv", "template_love17", senAdmin).then(
          (response) => {
            onClick();
          },
          (error) => {
            console.log("FAILED...", error);
          }
        );
      }
    }
  }
  return (
    <Form
      className="p-[80px] border-solid border-[#262626] rounded-[20px] flex flex-col  gap-[30px] mt-[170px]"
      onFinish={sendEmail}
      initialValues={formData}
      onValuesChange={(_, values) => setFormData(values)}
      form={form}
      layout="vertical"
      // action="mailto:abc@example.com?subject=Feedback"
      // method="post"
    >
      <div className="w-[100%] flex flex-col  gap-[14px]">
        <Form.Item name="message" className="w-[100%]">
          <StyledInput
            className=" bg-[#1A1A1A] text-white border-[#262626] placeholder:text-white px-[16px] py-[20px]"
            style={{ fontSize: "20px" }}
            placeholder="Nhập điều ước của pé vào đây nè!!!!!!"
            // rows={1}
          />
        </Form.Item>
      </div>
      <div className="w-[100%] flex flex-wrap justify-between gap-[14px] text-center">
        {!loading && (
          <Input
            type="submit"
            className="button max-md:w-[100%] w-fit h-fit bg-[#703BF7] text-white border-[#703BF7] py-[14px] px-[34px] text-[14px] font-[400] leading-[24px]"
            style={{
              fontSize: "25px",
              backgroundColor: "#FFFFFF",
              // backgroundImage: "-webkit-linear-gradient(180deg, #FFFFFF 0%, #6284FF 33%, #FF0000 66%, #ffffff 100%)",
              WebkitBackgroundClip: "text",
              WebkitTextFillColor: "transparent",
            }}
            value="Gửi lời ước"
          />
        )}
      </div>
    </Form>
  );
}
