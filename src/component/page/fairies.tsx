"use client";
import { Image } from "antd";
import "./name.css";
import { ReactTyped } from "react-typed";
export default function Fairies({ tab, setTab }: any) {
  return (
    <div>
      <Image className="cloud cloud-top" alt="cloud" preview={false} src="/images/Daco_4208229.png" />

      <Image alt="cloud" className="cloud cloud-bot" preview={false} src="/images/Daco_4208229.png" />
      <Image alt="cloud" className="cloud cloud-bot-left" preview={false} src="/images/Daco_4208229.png" />
      <Image alt="cloud" className="cloud cloud-bot-right" preview={false} src="/images/Daco_4208229.png" />

      <div className="fairies main">
        <div id="content-first" className="content p-3 ant-popover ant-popconfirm ant-popover-placement-top bg-white text-black">
          <ReactTyped
            strings={[
              "Hello Pé, thiên thần xinh xắn của anh !!!!!",
              "Hãy ngồi yên để  nghe a kể một câu chuyện cổ tích nè ☺️☺️ 😍",
              "Chuyện kể rằng một ngày rất xưa, Có một cô bé thiên thần rất xinh nhưng cũng rất ham chơi.",
              " Vì tiểu thiên thần ham chơi nên vào một này đã lạc trong ánh nắng dịu dàng đến với nhân gian 🧚🏻‍♀️🧚🧚‍♂️",
            ]}
            typeSpeed={90}
            style={{ fontSize: "17px", fontWeight: "bolder", textShadow: "1px 3px #a4a4a89c " }}
            startDelay={10000}
            backDelay={5000}
            onComplete={() => {
              setTimeout(() => {
                setTab(tab + 1);
              }, 10000);
            }}
          />
        </div>
        <Image alt="fairies" preview={false} src="/images/—Pngtree—christmas fairy cartoon character_14128086.png" />
      </div>
      <Image
        className="cloud cloud-main main"
        alt="fairies"
        preview={false}
        src="/images/Daco_4284506.png"
        style={{
          bottom: "20%",
          zIndex: 10,
        }}
      />
    </div>
  );
}
