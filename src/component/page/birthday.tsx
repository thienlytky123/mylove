"use client";
import { Button, Image, Input, Popover } from "antd";
import "./name.css";
import { ReactTyped } from "react-typed";
import { useEffect, useRef, useState } from "react";
import Promise from "../form/promise";
import { Human } from "@/data";

const radomAny = (min: number, max: number) => {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
};

export default function BirthDayPage({ data }: { data: Human }) {
  const old = new Date().getFullYear() - data?.born.getFullYear();
  const [show, setshow] = useState(false);
  const [showSt, setST] = useState(false);

  const [loading, setloading] = useState(false);

  const [showHandel, setShowHandel] = useState(false);
  const [showCandle, setShowCandle] = useState(true);
  const [showEnd, setShowEnd] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setST(true);
    }, 30000);
  }, []);

  return (
    <div className="w-full h-[100vh] bg-black" style={{ background: "black !important" }}>
      <div>
        {data.images.map((image, index) => {
          return (
            <Image
              key={index}
              className="image-down"
              alt="image"
              style={{
                borderRadius: "10%",
                left: `${radomAny(1, 90)}%`,
                width: `${radomAny(30, 200)}px`,
                backgroundSize: "cover",
                // height: `${radomAny(100, 400)}px`,
                top: `-${radomAny(30, 100)}%`,
                animation: ` down-image 15s ${radomAny(1, 7)}0s linear infinite,ringM 20s ${radomAny(0, 7)}s linear infinite alternate`,
              }}
              src={image}
            />
          );
        })}
      </div>

      <div>
        <div
          className="flex absolute"
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            top: "20%",
            left: "0%",
            position: "absolute",
            color: "white",
            zIndex: 100,
          }}
        >
          {old
            .toString()
            .split("")
            .map((x, index) => {
              return (
                <h1 className="text-3d" key={index}>
                  <input type="checkbox" />
                  <div style={{ animation: `glow ${radomAny(2, 4)}s linear infinite` }}>{x}</div>
                </h1>
              );
            })}
        </div>
        <div className="w-full h-[100vh] absolute bg-fire" style={{ background: "url('https://i.gifer.com/6k2.gif')" }}>
          <Image
            className="cloud"
            alt="fire"
            preview={false}
            style={{ top: "-2%", left: "-5%", width: "200px", transform: "scaleY(-1)" }}
            src="https://media.giphy.com/media/dZG24VVOfIZhhKhxLW/giphy.gif?cid=ecf05e47tc01qo9y88bwuq4o3jd260g9wt7d4g98sgzcyr0g&ep=v1_stickers_search&rid=giphy.gif&ct=s"
          />
          <Image
            className="cloud"
            alt="fire"
            preview={false}
            style={{ top: "-2%", right: "-5%", width: "200px", transform: "scale(-1)" }}
            src="https://media.giphy.com/media/dZG24VVOfIZhhKhxLW/giphy.gif?cid=ecf05e47tc01qo9y88bwuq4o3jd260g9wt7d4g98sgzcyr0g&ep=v1_stickers_search&rid=giphy.gif&ct=s"
          />
        </div>
        <Popover
          open={showSt}
          content={
            <div
              style={{
                position: "absolute",
                color: "white",
                top: "-60vh",
                width: "max-content",
                left: "500%",
                padding: "20px",
                backgroundColor: "#0a2be3a8",
                fontSize: "20px",
                borderRadius: "10px",
              }}
            >
              Nhấp vào bánh kem!!!
            </div>
          }
        >
          <Image
            className={showCandle ? "cloud candle" : "cloud candle hidden"}
            alt="cloud"
            preview={false}
            style={{ bottom: "20%", paddingBottom: "45px" }}
            src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExbDRteHFtYjJ2NWdpNGtxbmt3Z3Vpa2JoY2Qyc3l2ZWdvMGh1dHgxbCZlcD12MV9zdGlja2Vyc19zZWFyY2gmY3Q9cw/ArS6DkMXotqK5h99Tk/giphy.gif"
          />
          <Image
            className={showCandle ? "cloud cake" : "cloud cake cake-hidden"}
            alt="cloud"
            preview={false}
            style={{ bottom: "20%" }}
            onClick={() => {
              if (!loading) {
                setST(false);
                setshow(!show);
              }
            }}
            src="https://media.giphy.com/media/0aZRKivWvQqHYPKM95/giphy.gif?cid=ecf05e47qa2080thvlt72jhrcoggakqhp5iq4haxzywu24bq&ep=v1_stickers_search&rid=giphy.gif&ct=s"
          />
        </Popover>
      </div>
      {show && (
        <div
          className="w-full h-[100vh] bg-[rgb(255 255 255 / 21%)] absolute top-0 flex flex-col justify-center items-center gap-[30px]"
          style={{ zIndex: 200, position: "absolute", top: 0, background: "rgb(21 28 72 / 61%)", padding: "30px", gap: "90px" }}
        >
          <h1
            className=" !uppercase text-[15px] font-[700] leading-3 wrapper mb-[20px]"
            style={{
              backgroundColor: "#FFFFFF",
              backgroundImage: "-webkit-linear-gradient(180deg, #FFFFFF 0%, #6284FF 33%, #FF0000 66%, #ffffff 100%)",
              WebkitBackgroundClip: "text",
              WebkitTextFillColor: "transparent",
            }}
          >
            <span>H</span>
            <span>A</span>
            <span>P</span>
            <span>P</span>
            <span>Y</span>
            <span></span>
            <span>B</span>
            <span>I</span>
            <span>R</span>
            <span>T</span>
            <span>H</span>
            <span>D</span>
            <span>A</span>
            <span>Y</span>
          </h1>
          <Promise
            name={data.name}
            email={"asd@gmail.com"}
            onClick={() => {
              setloading(true);
              setshow(!show);
              setShowHandel(!showHandel);
            }}
          />
        </div>
      )}
      {showHandel && (
        <div
          onClick={() => {
            setShowCandle(!showCandle);
            setShowHandel(!showHandel);
          }}
          className="cloud "
          style={{ width: "100%", bottom: "0%", right: 0, textAlign: "right" }}
        >
          <p
            className="bg-white text-black"
            style={{
              position: "absolute",
              width: "40%",
              top: "-20%",
              padding: "10px",
              left: "20%",
              borderRadius: "10px 10px 0px 10px",
              fontSize: "17px",
              color: "red",
            }}
          >
            Thổi ném thôi Pé iu ơi!!
          </p>
          <Image
            alt="cloud"
            preview={false}
            style={{ bottom: "10%", right: 0, transform: "scaleX(-1)", width: "200px" }}
            src="https://media.giphy.com/media/6hby8X4u6ktkQ/giphy.gif?cid=ecf05e4781wm1m8jq2q6adftqxyemugkc1ztpova6wtvx3i1&ep=v1_stickers_search&rid=giphy.gif&ct=s"
          />
        </div>
      )}
      {!showCandle && (
        <div
          className="w-full h-[100vh] bg-[rgb(255 255 255 / 21%)] absolute top-0 flex flex-col justify-center items-center gap-[30px]"
          style={{
            zIndex: 200,
            position: "absolute",
            top: 0,
            background: "rgb(255 255 255 / 21%)",
            padding: "30px",
            gap: "30px",
            animation: "show-in 5s",
          }}
        >
          {showEnd ? (
            <h1 className="text-shadow" style={{ fontSize: "17px" }}>
              --Chúc Mừng sinh nhật--
              <br />
              -- Pé Iu của a --
              <br />
              ❤️‍🔥❤️‍🔥❤️‍🔥 Thương Pé !!!! ❤️‍🔥❤️‍🔥❤️‍🔥
            </h1>
          ) : (
            <ReactTyped
              className="text-effect"
              style={{ fontSize: "20px" }}
              strings={[
                "Chúc cô Pé thiên thần của anh một ngày sinh sinh nhật thật vui vẽ.",
                "Sinh nhật năm nay có hoa, có quà, có cả nến lung linh, có những đóa hoa tươi thắm và còn có tình yêu vô hạn mà anh dành cho Pé",
                "Chúc Pé, một nửa của đời anh luôn cảm thấy hạnh phúc khi có anh luôn bên cạnh.",
                "Mừng ngày sinh nhật của Pé, chúc Pé mãi giữ nụ cười trên môi, mãi xinh đẹp.",
                "Sông có thể cạn, núi có thể mòn nhưng tình yêu của anh luôn mãi sắc son",
                "Lời nói chẳng mất tiền mua. Lựa lời mà chúc: Sinh nhật vui vẻ nha Pé của anh",
              ]}
              typeSpeed={90}
              startDelay={10000}
              backDelay={5000}
              onComplete={() => {
                setShowEnd(true);
              }}
            />
          )}
        </div>
      )}
    </div>
  );
}
