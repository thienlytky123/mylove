"use client";
import { Image } from "antd";
import "./name.css";
import { ReactTyped } from "react-typed";
import Calendar from "../form/calendaPage";
export default function Home({ tab, setTab, date, imagePng }: any) {
  return (
    <div>
      <Image
        style={{ width: "120% !important", transform: "scale(2)", alignItems: "center", bottom: "20%", zIndex: 10 }}
        className="cloud house"
        alt="cloud"
        preview={false}
        src="/images/house.png"
      />
      <Image className="cloud cloud-top" style={{ left: "-60%" }} alt="cloud" preview={false} src="/images/Daco_4963073.png" />
      <Image className="cloud cloud-top" alt="cloud" style={{ right: "-60%" }} preview={false} src="/images/Daco_4963073.png" />
      <Calendar tab={tab} setTab={setTab} date={date} imagePng={imagePng} />
    </div>
  );
}
