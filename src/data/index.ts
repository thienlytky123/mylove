export interface Human {
  type: "love" | "friend" | "sister" | "brother" | "mon" | "father";
  name: string;
  born: Date;
  call: string;
  images: string[];
  png: string;
  wish: string[];
}
const thanh_hang: Human = {
  type: "love",
  name: "Nguyễn Thị Thanh Hằng",
  born: new Date("06/20/2001"),
  call: "Pé",
  png: "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/girl.png?alt=media&token=344d293c-9af7-4955-b14e-1cf4a7c3b434",
  images: [
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/426701614_3684752338411184_5581187332026347688_n.jpg?alt=media&token=fa2806be-7a7a-4718-9b65-71f37f0dc7db",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/414079749_3650990188454066_1837810426889198929_n.jpg?alt=media&token=73665a41-d66d-4791-a2c5-c817b7ca7beb",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/414068101_3650990325120719_6025752199752082648_n.jpg?alt=media&token=341f2af2-2ac6-49a7-989b-8e7a611c0dbd",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/401526002_3631806210372464_7990947122107248865_n.jpg?alt=media&token=76632715-d74e-47e0-9cb7-302f16a759c0",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/371443556_3576543605898725_2346995057640769353_n.jpg?alt=media&token=b6be7b00-2d7c-47f0-b00a-52597ff65e63",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/371321698_3576543542565398_8064375745626639983_n.jpg?alt=media&token=ea824b1d-6785-48fb-8e8f-8b9a3fa20038",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/354578447_3526705724215847_6597388124193942069_n.jpg?alt=media&token=ac0540eb-86dc-46dc-be12-802e43eb119e",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/335513431_1938878013121541_6659173215246603943_n.jpg?alt=media&token=1857913f-4a14-4e67-aa23-90e5cc159bd0",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/326041245_1968981003303848_8826128708570758858_n.jpg?alt=media&token=7f64f1c5-847b-4ab4-be65-f5c83d075254",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/301824933_3298262770393478_8979586540778505861_n.jpg?alt=media&token=733b93a3-9008-4dd7-9d11-d5d49dd263bc",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/271708684_3127882180764872_5650291337063018897_n.jpg?alt=media&token=f2f221f1-ce2d-4995-aba1-2eb14dea0906",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/167744788_2918784198341339_1644141733344727499_n.jpg?alt=media&token=89a5ff3b-5b20-4f25-870c-fdca457bdb91",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/159777548_2905315789688180_5775289149338170267_n.jpg?alt=media&token=f1dcf568-d781-450c-b77e-f62ef641b336",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/eca096fc-fbdf-4676-8ca8-856f1a2e60d0.jpeg?alt=media&token=e9860d55-8740-4cff-919d-4d89fca6b9d8",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/132660715_2849851525234607_4802233076422582103_n.jpg?alt=media&token=fae43542-b689-41e0-8534-4b3d4f93a1c7",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/e27380a5-9998-4d31-a864-32998ea4b5c2.jpeg?alt=media&token=4fc6773a-d3c4-4c17-b16e-377c58b6053d",
    "https://firebasestorage.googleapis.com/v0/b/mylove-abd80.appspot.com/o/426898045_3684752381744513_8725460678574709736_n.jpg?alt=media&token=17af75f8-9079-4141-859c-f0798a95fc55",
  ],
  wish: [""],
};
const Authentication: { pass: string; data: Human }[] = [
  {
    pass: "peiucuaanh-20062001",
    data: thanh_hang,
  },
];
export default function data(pass: string) {
  return Authentication.filter((authentication) => authentication.pass === pass)?.[0]?.data;
}
export const promise: [{ type: "love" | "friend" | "sister" | "brother" | "mon" | "father"; data: string[] }] = [
  {
    type: "love",
    data: [
      "Sinh nhật năm nay có hoa, có quà, có cả nến lung linh, có những đóa hoa tươi thắm và còn có tình yêu vô hạn mà anh dành cho Pé",
      "Chúc Pé, một nửa của đời anh luôn cảm thấy hạnh phúc khi có anh luôn bên cạnh.",
      "Mừng ngày sinh nhật của Pé, chúc Pé mãi giữ nụ cười trên môi, mãi xinh đẹp.",
      "Sông có thể cạn, núi có thể mòn nhưng tình yêu của anh luôn mãi sắc son",
      "Lời nói chẳng mất tiền mua. Lựa lời mà chúc: Sinh nhật vui vẻ nha Pé của anh",
    ],
  },
];
