"use client";
import BirthDayPage from "@/component/page/birthday";
import FistPage from "@/component/page/fairies";
import Home from "@/component/page/home";
import { Image } from "antd";
import { useRef, useState } from "react";
import dataS, { Human } from "@/data/index";
import { redirect, useParams } from "next/navigation";
import moment from "moment";
export default function Birthday() {
  const { password }: { password: string } = useParams();
  const data: Human = dataS(password || "");
  if (!password || !data) {
    redirect("/");
  }
  const today = moment(new Date()).startOf("day");
  if (today.valueOf() !== moment(data.born).set("year", today.year()).valueOf()) {
    // redirect("/");
  }
  const [show, setshow] = useState(false);
  const vidRef: any = useRef();
  const [tab, setTab] = useState(1);

  const play = () => {
    if (vidRef && vidRef.current) {
      vidRef.current.muted = false;
    }
  };
  return (
    <div
      className="w-full h-[100vh] bg-white "
      style={{
        background:
          "url('https://en.idei.club/uploads/posts/2023-06/thumbs/1686883081_en-idei-club-p-fairy-tale-background-dizain-krasivo-2.jpg') center/cover no-repeat",
      }}
    >
      {tab == 1 && <FistPage tab={tab} setTab={setTab} />}
      {tab == 2 && <Home tab={tab} setTab={setTab} date={data.born} imagePng={data.png} />}
      {tab === 3 && (
        <>
          <video style={{ opacity: 0, height: 0, width: 0 }} id="audio" autoPlay loop muted playsInline data-autoplay="" ref={vidRef}>
            <source src="/hbbd.mp4" type="video/mp4" />
          </video>

          {show ? (
            <div className="w-full h-[100vh] bg-black" style={{ background: "black !important" }}>
              <BirthDayPage data={data} />
            </div>
          ) : (
            <div className="w-full h-[100vh] bg-white flex flex-col justify-center z-100 items-center">
              <p className=" text-[30px] font-[700] text-blue-800">Bấm vào để bật loa lên nà</p>
              <Image
                src="/images/—Pngtree—mute icon_4419502.png"
                alt="muted"
                preview={false}
                onClick={() => {
                  play();
                  setshow(!show);
                }}
              />
            </div>
          )}
        </>
      )}
    </div>
  );
}
