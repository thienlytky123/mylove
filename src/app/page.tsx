"use client";
import { Button, Form, Image, Input, Modal, Popover } from "antd";
import { useState } from "react";

const Content = () => {
  const [formData, setFormData] = useState({ pass: "" });
  const [form] = Form.useForm();
  return (
    <Form
      className="p-[10px] border-solid border-[#262626] rounded-[20px] flex flex-col "
      // onFinish={sendEmail}
      style={{ backgroundColor: "transparent" }}
      initialValues={{ pass: "" }}
      onValuesChange={(_, values) => setFormData(values)}
      form={form}
      layout="vertical"
    >
      <Form.Item name={"pass"}>
        <Input type="text" />
      </Form.Item>

      <Button type="link" href={"./" + formData.pass}>
        Xong
      </Button>
    </Form>
  );
};
export default function Home() {
  const [open, setOpen] = useState(false);

  const handleOpenChange = (newOpen: boolean) => {
    setOpen(newOpen);
  };
  return (
    <main className="flex min-h-screen flex-col items-center justify-between ">
      <div
        className="w-full h-[100vh] flex flex-col justify-end text-end items-center pb-[20%]"
        // style={{
        //   background:
        //     "linear-gradient(to right, rgb(85 83 83 / 40%) 100%, rgb(44 66 240 / 3%) 100%) top right/100% 100% no-repeat, url('https://media.giphy.com/media/5N8NagF0xmyd1n41Md/giphy.gif?cid=790b76116dx4md5gjrf56lt4vqa8tqyar99uyz7qc39xi7sh&ep=v1_stickers_search&rid=giphy.gif&ct=s') top right/100% 100% no-repeat",
        // }}
      >
        <div className=" bg-white p-5 text-black rounded-lg text-left">
          <b>From:</b>TL
          <br />
          <p>Happy birthday</p>
        </div>
        <Popover
          style={{ backgroundColor: "transparent !important" }}
          content={<Content />}
          title="Nhập Password:"
          trigger="click"
          open={open}
          onOpenChange={handleOpenChange}
        >
          <Image
            className="image-custom"
            style={{
              height: "50vh !important",
              width: "unset !important",
              filter: "drop-shadow(5px 15px 5px #f5f0f0)",
              opacity: open ? 0.5 : 1,
            }}
            preview={false}
            alt="gift"
            src="https://media.giphy.com/media/QxMYJSkfpVHOZMEDdw/giphy.gif?cid=ecf05e47q9pic7myh5woxeu4of167lspcgznbtfepmeoolg1&ep=v1_stickers_search&rid=giphy.gif&ct=s"
          />
        </Popover>
      </div>
    </main>
  );
}
